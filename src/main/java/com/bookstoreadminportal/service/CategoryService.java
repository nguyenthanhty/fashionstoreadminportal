package com.bookstoreadminportal.service;

import com.bookstoreadminportal.domain.Category;

import java.util.List;

/**
 * Created by MinhToan on 6/21/2017.
 */

public interface CategoryService {

    Category save(Category category);

    Category findById(Long id);

    List<Category> findAll();

    void deleteCategory(Long id);


    Category getCategory(Long id);
}
