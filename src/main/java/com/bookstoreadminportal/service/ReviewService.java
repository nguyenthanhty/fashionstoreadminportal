package com.bookstoreadminportal.service;

import com.bookstoreadminportal.domain.Review;
import com.sun.org.apache.regexp.internal.RE;

import java.util.List;

/**
 * Created by Administrator on 6/30/2017.
 */
public interface ReviewService {
    Review findById(Long id);
    Review save(Review review);

    List<Review> findAll();
    Review findOne(Long id);

    void deleteReview(Long id);

}
