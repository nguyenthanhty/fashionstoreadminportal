package com.bookstoreadminportal.service;

import com.bookstoreadminportal.domain.SubCategory;

import java.util.List;

/**
 * Created by MinhToan on 7/5/2017.
 */
public interface SubCategoryService {
    SubCategory save(SubCategory subcategory);

    SubCategory getsubCategory(Long id);

    List<SubCategory> getAll();

    void delete(Long id);
}
