package com.bookstoreadminportal.service;

import com.bookstoreadminportal.domain.User;
import com.bookstoreadminportal.domain.security.UserRole;
import java.util.Set;


public interface UserService {

    User createUser(User user, Set<UserRole> userRoles) throws Exception;

    User save (User user);


}
