package com.bookstoreadminportal.service.impl;
import com.bookstoreadminportal.domain.Slide;
import com.bookstoreadminportal.repository.SlideRepository;
import com.bookstoreadminportal.service.SlideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MinhToan on 6/22/2017.
 */
@Service
public class SlideServiceImpl implements SlideService {
    @Autowired
    private SlideRepository slideRepository;


    @Override
    public Slide save(Slide slide) {
        return slideRepository.save(slide);
    }


    @Override
    public Slide findById(Long id) {

       return  slideRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        slideRepository.delete(id);
    }

    @Override
    public List<Slide> getAll() {
        return (List<Slide>) slideRepository.findAll();
    }
}
