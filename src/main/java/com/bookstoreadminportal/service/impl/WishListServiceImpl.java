package com.bookstoreadminportal.service.impl;
import com.bookstoreadminportal.domain.Product;
import com.bookstoreadminportal.domain.User;
import com.bookstoreadminportal.domain.WishList;
import com.bookstoreadminportal.repository.WishListRepository;
import com.bookstoreadminportal.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 7/1/2017.
 */
@Service
public class WishListServiceImpl implements WishListService {
    @Autowired
    private WishListRepository wishListRepository;
    @Override
    public void create(WishList wishlist, User user, Product product) {
        wishlist.setUser(user);
        wishlist.setProduct(product);
        wishListRepository.save(wishlist);
    }

    @Override
    public List<WishList> findAll() {
        return (List<WishList>) wishListRepository.findAll();
    }

    @Override
    public List<WishList> getAll() {
        return (List<WishList>) wishListRepository.findAll();
    }
}
