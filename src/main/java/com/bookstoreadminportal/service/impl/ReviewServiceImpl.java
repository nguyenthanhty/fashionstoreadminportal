package com.bookstoreadminportal.service.impl;

import com.bookstoreadminportal.domain.Review;
import com.bookstoreadminportal.repository.ReviewRepository;
import com.bookstoreadminportal.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 6/30/2017.
 */
@Service
public class ReviewServiceImpl implements ReviewService {
    @Autowired
    ReviewRepository reviewRepository;
    @Override
    public Review findById(Long id) {
        return reviewRepository.findOne(id);
    }

    @Override
    public Review save(Review review) {
        return reviewRepository.save(review);
    }

    @Override
    public List<Review> findAll() {
        return (List<Review>) reviewRepository.findAll();
    }

    @Override
    public Review findOne(Long id) {
        return reviewRepository.findOne(id);
    }

    @Override
    public void deleteReview(Long id) {
       reviewRepository.delete(id);

    }

}
