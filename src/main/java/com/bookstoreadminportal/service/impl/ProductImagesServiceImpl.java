package com.bookstoreadminportal.service.impl;

import com.bookstoreadminportal.domain.ProductImages;
import com.bookstoreadminportal.repository.ProductImagesRepository;
import com.bookstoreadminportal.service.ProductImagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MinhToan on 6/22/2017.
 */
@Service
public class ProductImagesServiceImpl implements ProductImagesService {
    @Autowired
    private ProductImagesRepository productImagesRepository;

    @Override
    public ProductImages save(ProductImages productImages) {
        return productImagesRepository.save(productImages);
    }

    @Override
    public List<ProductImages> getAll() {
        return (List<ProductImages>) productImagesRepository.findAll();
    }

    @Override
    public ProductImages findById(Long id) {
        return productImagesRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        productImagesRepository.deleleProductImages(id);
    }

    @Override
    public List<ProductImages> findByProductId(Long productId) {
        return productImagesRepository.findByProductId(productId);
    }
}
