package com.bookstoreadminportal.service.impl;

import com.bookstoreadminportal.domain.SubCategory;
import com.bookstoreadminportal.repository.SubCategoryRepository;
import com.bookstoreadminportal.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MinhToan on 7/5/2017.
 */
@Service
public class SubCategoryServiceImpl implements SubCategoryService {

    @Autowired
    private SubCategoryRepository subCategoryRepository;
    @Override
    public SubCategory save(SubCategory subcategory) {
        return subCategoryRepository.save(subcategory);
    }

    @Override
    public SubCategory getsubCategory(Long id) {
        return subCategoryRepository.findOne(id);
    }

    @Override
    public List<SubCategory> getAll() {
        return (List<SubCategory>) subCategoryRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        subCategoryRepository.delete(id);
    }
}
