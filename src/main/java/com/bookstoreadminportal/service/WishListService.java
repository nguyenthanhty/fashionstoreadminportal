package com.bookstoreadminportal.service;

import com.bookstoreadminportal.domain.Product;
import com.bookstoreadminportal.domain.User;
import com.bookstoreadminportal.domain.WishList;

import java.util.List;

/**
 * Created by Administrator on 7/1/2017.
 */
public interface WishListService {

    void create(WishList wishlist, User user, Product product);
    List<WishList> findAll();
    List<WishList> getAll();
}
