package com.bookstoreadminportal.service;

import com.bookstoreadminportal.domain.Product;

import java.util.List;

/**
 * Created by MinhToan on 6/22/2017.
 */
public interface ProductService {
    Product getProduct(Long id);

    Product save(Product product);

    List<Product> findAll();

    Product findOne(Long id);

    void delete(Long id);
}
