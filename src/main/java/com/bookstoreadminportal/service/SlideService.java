package com.bookstoreadminportal.service;


import com.bookstoreadminportal.domain.Slide;

import java.util.List;

/**
 * Created by MinhToan on 6/22/2017.
 */
public interface SlideService {

    Slide save(Slide slide);

    Slide findById(Long id);

    void  delete (Long id);

    List<Slide> getAll();

}
