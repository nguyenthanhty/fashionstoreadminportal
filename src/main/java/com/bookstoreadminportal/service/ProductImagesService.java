package com.bookstoreadminportal.service;

import com.bookstoreadminportal.domain.ProductImages;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by MinhToan on 6/22/2017.
 */
public interface ProductImagesService
{
    ProductImages save(ProductImages productImages);

    List<ProductImages> getAll();

    ProductImages findById(Long id);

    void delete(Long id);

    List<ProductImages> findByProductId(Long productId);

}
