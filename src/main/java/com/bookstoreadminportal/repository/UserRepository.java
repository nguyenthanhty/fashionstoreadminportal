package com.bookstoreadminportal.repository;

import com.bookstoreadminportal.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by MinhToan on 6/16/2017.
 */
public interface UserRepository extends CrudRepository<User,Long> {

    User findByUsername(String username);
}
