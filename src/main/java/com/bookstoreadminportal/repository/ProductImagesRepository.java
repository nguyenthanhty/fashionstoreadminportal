package com.bookstoreadminportal.repository;

import com.bookstoreadminportal.domain.ProductImages;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by MinhToan on 6/22/2017.
 */
public interface ProductImagesRepository extends CrudRepository<ProductImages,Long>{
    @Modifying
    @Query("delete from ProductImages s where s.id = :id")
    @Transactional
    void deleleProductImages (@Param("id") Long id);

    List<ProductImages> findByProductId(Long productId);
}
