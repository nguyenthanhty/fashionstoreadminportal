package com.bookstoreadminportal.repository;


import com.bookstoreadminportal.domain.WishList;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Administrator on 7/1/2017.
 */
public interface WishListRepository extends CrudRepository<WishList,Long > {
}
