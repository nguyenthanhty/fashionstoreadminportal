package com.bookstoreadminportal.repository;

import com.bookstoreadminportal.domain.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by MinhToan on 6/21/2017.
 */
public interface CategoryRepository extends CrudRepository<Category,Long> {



}
