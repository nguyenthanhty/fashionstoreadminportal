package com.bookstoreadminportal.repository;

import com.bookstoreadminportal.domain.Product;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by MinhToan on 6/22/2017.
 */
public interface ProductRepository extends CrudRepository<Product,Long>{
    @Modifying
    @Transactional
    @Query("delete from Product s where s.id = :id")
    void deleteById(@Param("id") Long id);

}
