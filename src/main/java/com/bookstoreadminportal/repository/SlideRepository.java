package com.bookstoreadminportal.repository;

import com.bookstoreadminportal.domain.Slide;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by MinhToan on 6/22/2017.
 */
public interface SlideRepository extends CrudRepository<Slide,Long> {

}
