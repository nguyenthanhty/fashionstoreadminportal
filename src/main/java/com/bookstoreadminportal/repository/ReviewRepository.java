package com.bookstoreadminportal.repository;

import com.bookstoreadminportal.domain.Review;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Administrator on 6/30/2017.
 */
public interface ReviewRepository extends CrudRepository<Review,Long> {
}
