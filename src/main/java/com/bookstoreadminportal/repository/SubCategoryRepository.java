package com.bookstoreadminportal.repository;

import com.bookstoreadminportal.domain.SubCategory;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by MinhToan on 7/5/2017.
 */
public interface SubCategoryRepository extends CrudRepository<SubCategory,Long> {
    @Modifying
    @Transactional
    @Query("delete from SubCategory s where s.id = :id")
    void deleteById(@Param("id") Long id);

}
