package com.bookstoreadminportal.domain;

import javax.persistence.*;

/**
 * Created by MinhToan on 6/21/2017.
 */
@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Category(){

    }

    public Category(String name) {
        this.name = name;
    }
}
