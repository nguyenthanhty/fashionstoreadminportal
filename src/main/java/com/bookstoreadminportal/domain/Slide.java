package com.bookstoreadminportal.domain;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * Created by MinhToan on 6/23/2017.
 */
@Entity
@Table(name = "slide")
public class Slide {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String urlImage;
    @Transient
    private MultipartFile slideImage;

    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public MultipartFile getSlideImage() {
        return slideImage;
    }

    public void setSlideImage(MultipartFile slideImage) {
        this.slideImage = slideImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
