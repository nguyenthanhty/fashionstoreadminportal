package com.bookstoreadminportal.domain;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by MinhToan on 6/16/2017.
 */
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;
    private String name;
    private String branch;
    private String color;
    private String size;
    private double price;
    private double discount;
    private String date;
    private String isAvailable;
    private String gender;
    private boolean active=true;
    private String urlImage;
    private String status;
    private int inStockNumber;

    @OneToMany(mappedBy = "product",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Set<ProductImages> productImages;


    @ManyToOne(targetEntity = SubCategory.class)
    @JoinColumn(name = "sub_category_id",referencedColumnName = "id" )
    public SubCategory subCategory;

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    @Column(columnDefinition = "text")
    private String description;
    @Transient
    private MultipartFile bookImage;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public int getInStockNumber() {
        return inStockNumber;
    }

    public void setInStockNumber(int inStockNumber) {
        this.inStockNumber = inStockNumber;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(String isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<ProductImages> getProductImages() {
        return productImages;
    }

    public void setProductImages(Set<ProductImages> productImages) {
        this.productImages = productImages;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MultipartFile getBookImage() {
        return bookImage;
    }

    public void setBookImage(MultipartFile bookImage) {
        this.bookImage = bookImage;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public Product(){
    }

    public Product(String name, String branch, String color, String size, double price, double discount, String date, String isAvailable, String gender, boolean active, String urlImage, String status, int inStockNumber, Set<ProductImages> productImages, SubCategory subCategory, String description, MultipartFile bookImage) {
        this.name = name;
        this.branch = branch;
        this.color = color;
        this.size = size;
        this.price = price;
        this.discount = discount;
        this.date = date;
        this.isAvailable = isAvailable;
        this.gender = gender;
        this.active = active;
        this.urlImage = urlImage;
        this.status = status;
        this.inStockNumber = inStockNumber;
        this.productImages = productImages;
        this.subCategory = subCategory;
        this.description = description;
        this.bookImage = bookImage;
    }
}
