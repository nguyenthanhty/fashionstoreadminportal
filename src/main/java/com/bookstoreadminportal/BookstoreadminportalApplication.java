package com.bookstoreadminportal;
import com.bookstoreadminportal.domain.User;
import com.bookstoreadminportal.domain.security.Role;
import com.bookstoreadminportal.domain.security.UserRole;
import com.bookstoreadminportal.service.UserService;
import com.bookstoreadminportal.utility.SercurityUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class BookstoreadminportalApplication implements CommandLineRunner {

	@Autowired
	private UserService userservice;

	public static void main(String[] args)  {

		SpringApplication.run(BookstoreadminportalApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		User user = new User();
		user.setFirstName("a");
		user.setLastName("admin");
		user.setUsername("admin");
		user.setPhone("0935215089");
		user.setPassword(SercurityUtility.passwordEncoder().encode("admin"));
		user.setEmail("hoangminhtoan1993@gmail.com");
		Set<UserRole> userRoles = new HashSet<>();
		Role role1= new Role();
		role1.setRoleId(1);
		role1.setName("ROLE_ADMIN");
		userRoles.add(new UserRole(user,role1));

		userservice.createUser(user,userRoles);

	}
}
