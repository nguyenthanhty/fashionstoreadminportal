package com.bookstoreadminportal.controller;

import com.bookstoreadminportal.domain.Category;
import com.bookstoreadminportal.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by MinhToan on 6/21/2017.
 */
@Controller
@RequestMapping(value = "/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping(value = "/add")
    private String saveCategory(Model model) {

        Category category = new Category();
        model.addAttribute("category", category);

        return "addCategory";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    private String saveCategory(@ModelAttribute("category") Category category) {

        categoryService.save(category);
        return "redirect:categoryList";
    }

    @RequestMapping(value = "/categoryList", method = RequestMethod.GET)
    public String listCategory(Model model) {

        List<Category> listCategory = categoryService.findAll();
        model.addAttribute("listCategory", listCategory);
        return "categoryList";
    }

    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public String deleteCategory(@RequestParam("id") long id) {

        categoryService.deleteCategory(id);
        return"redirect:categoryList";
    }

}
