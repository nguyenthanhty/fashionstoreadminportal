package com.bookstoreadminportal.controller;

import com.bookstoreadminportal.domain.Product;
import com.bookstoreadminportal.domain.ProductImages;
import com.bookstoreadminportal.service.ProductImagesService;
import com.bookstoreadminportal.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.util.List;
import java.util.UUID;

/**
 * Created by MinhToan on 6/22/2017.
 */
@Controller
@RequestMapping(value = "/productimages")
public class ProductImagesController {
    @Autowired
    private ProductImagesService productImagesService;

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/delete")
    public String deleteProductImage(@RequestParam("id") Long id) {
        productImagesService.delete(id);
        return "redirect:/product/productList";
    }

    @RequestMapping(value = "/updateimages", method = RequestMethod.GET)
    public String addProductImages(Model model,@RequestParam("id") Long id) {
//        ProductImages productImages = new ProductImages();
        List<ProductImages> productImages = productImagesService.getAll();
        Product product = productService.findOne(id);

        ProductImages productImage = new ProductImages();
        productImage.setProduct(product);
//        Product product = productService.findOne(productImages.getProduct().getId());
        model.addAttribute("productImages", productImages);
        model.addAttribute("productImage", productImage);
        model.addAttribute("product",product);
        return "addProductImages";
    }

    @RequestMapping(value = "/updateimages", method = RequestMethod.POST)
    public String addProductImages(@ModelAttribute("productImages") ProductImages productImages,@RequestParam("id") Long productId, HttpServletRequest request) throws MalformedURLException {

        String url = "http://localhost:8082/adminportal/image/book/productimage/";
        String name = UUID.randomUUID().toString() + ".png";

        MultipartFile images = productImages.getProductimages();
        try {
            byte[] bytes = images.getBytes();
            BufferedOutputStream outputStream = new BufferedOutputStream(
                    new FileOutputStream(new File("src/main/resources/static/image/book/productimage/" + name)));

            outputStream.write(bytes);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*productImages.setUrlImage(url + name);
        productImages.setProduct(productService.getProduct(productId));*/

        ProductImages productImages1 = new ProductImages();
        productImages1.setProduct(productService.getProduct(productId));
        productImages1.setUrlImage(url+name);
        productImagesService.save(productImages1);

        return "redirect:/product/productList";
    }

//    @ModelAttribute("productimages")
//    public List<ProductImages> getListProductImages(Model model,HttpServletRequest request) throws MalformedURLException {
//
//        String Url = PortContextPath.getURLBase(request);
//        List<ProductImages> listProductImages = productImagesService.getAll();
//        model.addAttribute("listProductImages", listProductImages);
//        model.addAttribute("Url",Url);
//
//        return listProductImages;
//    }
}
