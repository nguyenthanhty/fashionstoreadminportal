package com.bookstoreadminportal.controller;

import com.bookstoreadminportal.domain.Category;
import com.bookstoreadminportal.domain.SubCategory;
import com.bookstoreadminportal.service.CategoryService;
import com.bookstoreadminportal.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by MinhToan on 7/5/2017.
 */
@Controller
@RequestMapping(value = "/subcategory")
public class SubCategoryController {
    @Autowired
    private SubCategoryService subcategoryService;
    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public String addsubCategory(Model model){
        List<Category> categoryList = categoryService.findAll();
        model.addAttribute("categoryList",categoryList);
        return "addSubCategory";
    }
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String addsubCategory(@RequestParam("categoryId") Long categoryId, @RequestParam("nameSubCategory") String nameSubCategory){
        Category category = categoryService.getCategory(categoryId);
        SubCategory subCategory = new SubCategory();
        subCategory.setName(nameSubCategory);
        subCategory.setCategory(category);
        subcategoryService.save(subCategory);
        return "listSubCategory";
    }

    @RequestMapping(value = "/listSubCategory",method = RequestMethod.GET)
    public String getListSubCategory(Model model){

        List<SubCategory> subCategoryList = subcategoryService.getAll();
        model.addAttribute("subCategoryList",subCategoryList);
        return "listSubCategory";
    }
    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public String deleteSubCatgory(@RequestParam("id") Long id){
        subcategoryService.delete(id);
        return "redirect:listSubCategory";
    }
}
