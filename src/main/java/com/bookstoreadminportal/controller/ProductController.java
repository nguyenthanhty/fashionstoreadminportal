package com.bookstoreadminportal.controller;

import com.bookstoreadminportal.domain.Category;
import com.bookstoreadminportal.domain.Product;
import com.bookstoreadminportal.domain.SubCategory;
import com.bookstoreadminportal.service.CategoryService;
import com.bookstoreadminportal.service.ProductService;
import com.bookstoreadminportal.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.List;
import java.util.UUID;

/**
 * Created by MinhToan on 6/22/2017.
 */
@Controller
@RequestMapping(value = "/product")
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SubCategoryService subCategoryService;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addProduct(ModelMap modelMap) {

        Product product = new Product();
        modelMap.addAttribute("product", product);

        List<SubCategory> subCategoryList = subCategoryService.getAll();
        modelMap.addAttribute("subCategoryList", subCategoryList);
        return "addProduct";
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addProduct(@ModelAttribute("product") Product product, HttpServletRequest httpServletRequest) throws Exception {
        String Url = "http://localhost:8082/adminportal/image/book/";
        String name = UUID.randomUUID().toString() + ".png";
        product.setUrlImage(Url+name);
        productService.save(product);
        MultipartFile bookImage = product.getBookImage();
        byte[] bytes = bookImage.getBytes();
        BufferedOutputStream stream = new BufferedOutputStream
                (new FileOutputStream(
                        new File("src/main/resources/static/image/book/" + name)));
        stream.write(bytes);
        stream.close();
        return "redirect:/product/productList";
    }

    @RequestMapping(value = "/updateProduct", method = RequestMethod.GET)
    public String updateProduct(@RequestParam("id") Long id, ModelMap modelMap) {

        Product product = productService.findOne(id);
        modelMap.addAttribute("product", product);
        List<SubCategory> subCategoryList = subCategoryService.getAll();
        modelMap.addAttribute("subCategoryList", subCategoryList);
        return "updateProduct";
    }

    @RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
    public String updateBook(@ModelAttribute("product") Product product, HttpServletRequest httpServletRequest) throws IOException {
        Product productItem = productService.findOne(product.getId());
        productItem.setName(product.getName());
        productItem.setBranch(product.getBranch());
        productItem.setColor(product.getColor());
        productItem.setGender(product.getGender());
        productItem.setDiscount(product.getDiscount());
        productItem.setSize(product.getSize());
        productItem.setDescription(product.getDescription());
        productItem.setIsAvailable(product.getIsAvailable());
        productItem.setPrice(product.getPrice());
        productItem.setStatus(product.getStatus());
        productItem.setInStockNumber(product.getInStockNumber());

        //Files.delete(Paths.get(product.getUrlImage()));
        String url = "http://localhost:8082/adminportal/image/book/";
        String name = UUID.randomUUID().toString() + ".png";
        product.setUrlImage(url + name);

        MultipartFile bookImage = product.getBookImage();

        if (!bookImage.isEmpty()) {
            try {
                byte[] bytes = bookImage.getBytes();

                BufferedOutputStream outputStream = new BufferedOutputStream(
                        new FileOutputStream(new File(
                                "src/main/resources/static/image/book/" + name)));
                outputStream.write(bytes);
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        productService.save(productItem);
//        return"redirect:/book/bookInfo?id="+book.getId();
        return "redirect:productList";
    }

    @RequestMapping(value = "/productList", method = RequestMethod.GET)
    public String listBook(ModelMap modelMap) throws  Exception {

        List<Product> productList = productService.findAll();
        modelMap.addAttribute("productList", productList);
        return "productList";
    }


    @RequestMapping(value = "/productInfo", method = RequestMethod.GET)
    public String productInfo(@RequestParam("id") Long id, ModelMap modelMap) {
        Product product = productService.findOne(id);
        modelMap.addAttribute("product", product);
        return "productInfo";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String delete(@RequestParam("id") Long id) {

        productService.delete(id);
        return "redirect:productList";
    }


}
