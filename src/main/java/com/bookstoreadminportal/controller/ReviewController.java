package com.bookstoreadminportal.controller;

import com.bookstoreadminportal.domain.Review;
import com.bookstoreadminportal.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 6/30/2017.
 */
@Controller
@RequestMapping(value = "/review")
public class ReviewController {
    @Autowired
    ReviewService reviewService;
    @RequestMapping(value = "/reviewList", method = RequestMethod.GET)
    public String listReview(Model model) {

        List<Review> listReview = reviewService.findAll();
        model.addAttribute("listReview", listReview);
        return "reviewList";
    }
    @RequestMapping(value = "/updateReview", method = RequestMethod.GET)
    public String updateReview(@RequestParam("id") Long id, ModelMap modelMap) {

        Review review = reviewService.findOne(id);
        modelMap.addAttribute("review", review);
        List<Review> reviewList =reviewService.findAll();
        modelMap.addAttribute("reviewList",reviewList);
        return "updateReview";
    }

    @RequestMapping(value = "/updateReview",method = RequestMethod.POST)
    public String updateReview(@ModelAttribute("review") Review review, HttpServletRequest httpServletRequest) throws IOException {
        Review reviewitem = reviewService.findOne(review.getId());

        reviewitem.setRating(review.getRating());
        reviewitem.setComment(review.getComment());
        reviewService.save(reviewitem);

//        return"redirect:/book/bookInfo?id="+book.getId();
        return"redirect:reviewList";
    }

    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public String deleteCategory(@RequestParam("id") long id) {

        reviewService.deleteReview(id);
        return"redirect:reviewList";
    }
    @RequestMapping(value = "/reviewInfo",method = RequestMethod.GET)
    public String reviewInfo(@RequestParam("id") Long id, ModelMap modelMap){
        Review review = reviewService.findOne(id);
        modelMap.addAttribute("review",review);
        return "reviewInfo";
    }
}
