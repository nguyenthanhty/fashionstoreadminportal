package com.bookstoreadminportal.controller;

import com.bookstoreadminportal.domain.WishList;
import com.bookstoreadminportal.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by Administrator on 7/3/2017.
 */
@Controller
@RequestMapping(value = "/wishlist")
public class WishListController {
    @Autowired
    WishListService wishListService;
    @RequestMapping(value = "/wishList", method = RequestMethod.GET)
    public String Listwish(Model model) {

        List<WishList> Listwish = wishListService.findAll();
        model.addAttribute("Listwish", Listwish);
        return "wishList";
    }
}
