package com.bookstoreadminportal.controller;

import com.bookstoreadminportal.domain.Slide;
import com.bookstoreadminportal.service.SlideService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.UUID;

/**
 * Created by MinhToan on 6/22/2017.
 */
@Controller
@RequestMapping(value = "/slide")
public class SlidesController {
    @Autowired
    private SlideService slidesService;

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public String addSlideṣ̣̣(Model model){
        Slide slide = new Slide();
        model.addAttribute("slide",slide);
        return "addSlide";
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public String addSlides(@ModelAttribute("slide")Slide slide, HttpServletRequest request){

        String url="http://localhost:8082/adminportal/image/slides/";
        String name= UUID.randomUUID().toString() + ".png";
        slide.setUrlImage(url+name);
        slidesService.save(slide);

        MultipartFile bookImages =slide.getSlideImage();

        try{
            byte[] bytes = bookImages.getBytes();

            BufferedOutputStream outputStream = new BufferedOutputStream(
                    new FileOutputStream(
                            new File
                                    ("src/main/resources/static/image/slides/"+name)));
            outputStream.write(bytes);
            outputStream.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return "redirect:slideList";

    }

    @RequestMapping(value = "/delete",method = RequestMethod.GET)
    public String deleteSlides(@RequestParam("id") Long id){
        slidesService.delete(id);
        return "redirect:slideList";
    }

    @RequestMapping(value = "/slideList",method = RequestMethod.GET)
    public String listSilde(Model model){

        List<Slide> listSlide = slidesService.getAll();
        model.addAttribute("listSlide",listSlide);
        return"slideList";
    }
}
