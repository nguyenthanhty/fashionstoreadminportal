package com.bookstoreadminportal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by MinhToan on 6/16/2017.
 */
@Controller
public class HomeController {
    public String index(){

        return "redirect:/home";
    }
    @RequestMapping(value = "/home")
    public String home(){

        return "home";
    }

    @RequestMapping(value = "/login")
    public String login()
    {
        return "login";
    }
}
